@extends('layouts.app')
@section('content')
@include('header')
<section class="map-section">
   <iframe id="map-change" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.2714999045634!2d106.83171581471971!3d-6.22789136272644!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3ee3b71b375%3A0xc4c1418c67517be2!2sPT%20INDOIN%20BUSINESS%20GROUP!5e0!3m2!1sen!2sin!4v1588912863932!5m2!1sen!2sin" width="100%" height="465" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</section>
<section class="contact-details">
   <div class="container">
      <div class="row">
         <div class="col-lg-10 m-auto">
            <div class="contact-details-box">
               <ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
                  <li class="nav-item">
                     <a class="nav-link active" id="pills-indonesia-tab" data-toggle="pill" href="#pills-indonesia" role="tab" aria-controls="pills-indonesia" aria-selected="true">INDONESIA</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" id="pills-india-tab" data-toggle="pill" href="#pills-india" role="tab" aria-controls="pills-india" aria-selected="false">india</a>
                  </li>
               </ul>
               <div class="tab-content" id="pills-tabContent">
                  <div class="tab-pane fade show active" id="pills-indonesia" role="tabpanel" aria-labelledby="pills-indonesia-tab">
                     <div class="row"> 
                        <div class="col-lg-3">
                           <p><b>Address</b>
                              <br><br>
                              PT Indoin Business Group <br>
                              Palma One Building <br>
                              2nd Floor - Suite 205 <br>
                              JL.Hr. Rasuna Said Kav. C2 No. 4 <br>
                              Jakarta, Indonesia 12950
                           </p>
                        </div>
                        <div class="offset-lg-1 col-lg-4">
                           <p><b>Social</b></p>
                           <ul class="social-reach-list">
                              <li><a href="https://www.facebook.com/Indoin-Business-Group-600689813415378/"><img src="/assets/img/facebook-color.png" alt=""><span>Indoin Business Group</span></a></li>
                              <li><a href="https://twitter.com/GroupIndoin"><img src="/assets/img/twitter-color.png" alt=""><span>@GroupIndoin</span></a></li>
                              <li><a href="https://www.linkedin.com/authwall?trk=gf&trkInfo=AQHaXrg29Jf0HQAAAXIB634wOZt4675DmpDNzgEe0WbeqBFC2lPPUxUYZOWq9uz1nZLs2nhULaB5fEBJcFFaIAfH4FGDMijQPz0dJ15zkSoINDjFynnEk0cGcNsj4iDKrrdHrXo=&originalReferer=http://www.indoingroup.com/contact-us&sessionRedirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2Findoin-business-group%2F"><img src="/assets/img/linkedin-color.png" alt=""><span>Indoin Business Group</span></a></li>
                              <li><a href="https://www.instagram.com/indoinbusinessgroup/?hl=id"><img src="/assets/img/instagram-color.png" alt=""><span>indoinbusinessgroup</span></a></li>
                           </ul>
                        </div>
                        <div class=" col-lg-4">
                           <p><b>Reach Us</b></p>
                           <ul class="reach-contact">
                              <li><a href="#">Phone: +62.21.522.8322</a></li>
                              <li><a href="#">Email: contact@indoingroup.com</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="pills-india" role="tabpanel" aria-labelledby="pills-india-tab">
                     <div class="row">
                        <div class="col-lg-3">
                           <p><b>Address</b>
                              <br><br>
                              Indoin Biolife Pvt. Ltd. 308-309, Shahpuri Tirath Singh TowerC-58, Janakpuri, Pin: 110 058 New Delhi, India
                           </p>
                        </div>
                        <div class="col-lg-3">
                           <p><b>Production Unit</b>
                              <br><br>
                              Indoin Biolife Pvt. Ltd. <br>
                              Talod Road, Dhansura, Taluka <br>
                              Dhansura, Dist Aravalli <br>
                              Gujarat, India -383310
                           </p>
                        </div>
                        <div class=" col-lg-3">
                           <p><b>Social</b></p>
                           <ul class="social-reach-list">
                              <li><a href="https://www.facebook.com/Indoin-Business-Group-600689813415378/"><img src="/assets/img/facebook-color.png" alt=""><span>Indoin Business Group</span></a></li>
                              <li><a href="https://twitter.com/GroupIndoin"><img src="/assets/img/twitter-color.png" alt=""><span>@GroupIndoin</span></a></li>
                              <li><a href="https://www.linkedin.com/authwall?trk=gf&trkInfo=AQHaXrg29Jf0HQAAAXIB634wOZt4675DmpDNzgEe0WbeqBFC2lPPUxUYZOWq9uz1nZLs2nhULaB5fEBJcFFaIAfH4FGDMijQPz0dJ15zkSoINDjFynnEk0cGcNsj4iDKrrdHrXo=&originalReferer=http://www.indoingroup.com/contact-us&sessionRedirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2Findoin-business-group%2F"><img src="/assets/img/linkedin-color.png" alt=""><span>Indoin Business Group</span></a></li>
                              <li><a href="https://www.instagram.com/indoinbusinessgroup/?hl=id"><img src="/assets/img/instagram-color.png" alt=""><span>indoinbusinessgroup</span></a></li>
                           </ul>
                        </div>
                        <div class=" col-lg-3">
                           <p><b>Reach Us</b></p>
                           <ul class="reach-contact">
                              <li><a href="#">Phone: +91 11 4300 4612</a></li>
                              <li><a href="#">Email: contact@indoingroup.com</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="contact-us-form pb-0">
   <div class="container">
      <div class="row">
         <div class="col-lg-9 m-auto">
            <div class="row">
               <div class="col-lg-12 text-center pb-lg-4">
                  <h1 class="main-small-heading">Contact Us</h1>
                  <div class="content">
                     <p class="pr-0">Please use the form below to Contact.</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-6">
                  <form>
                     <div class="form-group">
                        <input type="text" placeholder="Name" class="form-control">
                     </div>
                     <div class="form-group">
                        <input type="email" placeholder="Email" class="form-control">
                     </div>
                     <div class="form-group">
                        <input type="text" placeholder="Phone" class="form-control">
                     </div>
                     <div class="form-group">
                        <input type="text" placeholder="Company" class="form-control">
                     </div>
                  </form>
               </div>
               <div class="col-lg-6">
                  <textarea name="name" class="form-control" placeholder="Message" rows="9" cols="80"></textarea>
               </div>
            </div>
            <div class="row pt-1">
               <div class="col-lg-12">
                  <div class="text-center">
                     <button type="submit" class="btn btn-green">Submit</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="newsletter-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-11 m-auto">
            <div class="subscribe-box mt-0 p-0">
               <h4>Subscribe to get the latest updates</h4>
               <div class="subscribe-form">
                  <div class="row">
                     <div class="col-lg-8 col-sm-8 pl-sm-5">
                        <form>
                           <div class="form-group">
                              <input type="email" class="form-control" placeholder="Email">
                           </div>
                        </form>
                     </div>
                     <div class="pl-0 col-lg-4 col-sm-4">
                        <button type="submit" class="btn btn-green">Submit</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="last-section">
   <div class="footer-bg-color">
      <div class="container">
         <div class="footer-section">
            <div class="row">
               <div class="col-lg-3 col-sm-6">
                  <div class="footer-logo">
                     <img src="/assets/img/main-logo.png" alt="">
                  </div>
                  <p>Indoin is committed to Deliver Real Value to Customers in the field of Agrochemicals and ecofriendly bio products, by changing perception of Brand from High Price to High Value.</p>
                  <div class="social-icons">
                     <ul>
                        <li class="pl-0">
                           <a target="_blank" href="https://www.facebook.com/Indoin-Business-Group-600689813415378/"><img src="/assets/img/facebook-color.png" alt=""></a>
                        </li>
                        <li>
                           <a target="_blank" href="https://www.linkedin.com/company/indoin-business-group/"><img src="/assets/img/linkedin-color.png" alt=""></a>
                        </li>
                        <li>
                           <a target="_blank" href="https://twitter.com/GroupIndoin"><img src="/assets/img/twitter-color.png" alt=""></a>
                        </li>
                        <li>
                           <a target="_blank" href="https://www.instagram.com/indoinbusinessgroup/?hl=id"><img src="/assets/img/instagram-color.png" alt=""></a>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class=" col-lg-3 col-sm-6">
                  <h6>Head Office</h6>
                  <div class="footer-info">
                     <p>    <img src="/assets/img/loaction-icon.png" alt=""> <span>PT Indoin Business Group</span>
                        <br> <span class="pl-lg-3 pl-0">Palma One Building</span>
                        <br> <span  class="pl-lg-3 pl-0">2nd Floor - Suite 205</span>
                        <br> <span  class="pl-lg-3 pl-0">JL.Hr. Rasuna Said Kav. C2 No. 4</span>
                        <br> <span  class="pl-lg-3 pl-0">Jakarta, Indonesia 12950</span>
                     </p>
                     <a href="javascript:;">   <img src="/assets/img/phone-icon.png" alt="">   +62.21.522.8322</a>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <h6>India Office</h6>
                  <div class="footer-info">
                     <p>   <img src="/assets/img/loaction-icon.png" alt="">
                        <span> Indoin Biolife Pvt. Ltd.</span>
                        <br> <span class="pl-lg-3 pl-0">308-309, Shahpuri Tirath Singh </span>
                        <br> <span class="pl-lg-3 pl-0">TowerC-58, Janakpuri, Pin: 110 058</span>
                        <br> <span class="pl-lg-3 pl-0">New Delhi, India</span>
                     </p>
                     <a href="javascript:;">   <img src="/assets/img/phone-icon.png" alt=""> +91 11 4300 4612</a>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <h6>Production Unit</h6>
                  <div class="footer-info">
                     <p>   <img src="/assets/img/loaction-icon.png" alt="">
                        <span> Indoin Biolife Pvt. Ltd.</span>
                        <br> <span class="pl-lg-3 pl-0">Talod Road, Dhansura, Taluka  </span>
                        <br> <span class="pl-lg-3 pl-0">Dhansura, Dist Aravalli</span>
                        <br> <span class="pl-lg-3 pl-0">Gujarat, India -383310</span>
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="footer-bottom">
         <div class="container">
            <div class="footer-links">
               <div class="row">
                  <div class="col-lg-12 text-center">
                     <ul>
                        <li>© 2020 Indoin</li>
                        <li>
                           <a href="#">Privacy policy</a>
                        </li>
                        <li>
                           <a href="#">Term of service</a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
