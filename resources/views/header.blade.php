<div class="header-fixed">
   <header class="main-navigation pt-3 pb-3">
      <div class="container">
         <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="/"><img src="/assets/logos/logo-orange.jpeg" class="main-logo" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navigation" aria-controls="main-navigation" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="main-navigation">
               <ul class="navbar-nav ml-auto">
                  <li class="nav-item {{Request::segment(1) == ''?'active':''}}">
                     <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item {{Request::segment(1) == 'our-story'?'active':''}}">
                     <a class="nav-link" href="/our-story">Our Story</a>
                  </li>
                  <li class="nav-item {{Request::segment(1) == 'packages'?'active':''}}">
                     <a class="nav-link" href="/packages">Packages</a>
                  </li>

                  <li class="nav-item {{Request::segment(1) == 'contact-us'?'active':''}}">
                     <a class="nav-link" href="/contact-us">Contact Us</a>
                  </li>
               </ul>
            </div>
            <div class="">
               <div class="cl-select">
                  <ul>
                     <li class="cl-main-li">
                        <div class="dropdown">
                           <div class="country-dropdwon language-dropdwon dropdown-toggle" data-toggle="dropdown">
                              <img src="/assets/img/earth_icon.png" alt="">
                              <span><b>IN</b></span> -
                              <span><b>EN</b></span>
                              <i class="fa fa-sort-desc" aria-hidden="true"></i>
                              <ul class="dropdown-menu">
                                 <p class="country-text">Asia & Pacific</p>
                                 <li>
                                    <div class="row mt-1">
                                       <div class="col-4">
                                          <a class="country-name-dropdown" href="javascript:;">Indonesia</a>
                                       </div>
                                       <div class="col-8">
                                          <ul class="language-list">
                                             <li class="">
                                                <a href="#">English</a>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                    <div class="row mt-1">
                                       <div class="col-4">
                                          <a class="country-name-dropdown" href="javascript:;">India</a>
                                       </div>
                                       <div class="col-8">
                                          <ul class="language-list">
                                             <li class="">
                                                <a href="#">English</a>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </nav>
      </div>
   </header>
</div>
