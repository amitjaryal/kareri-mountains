@extends('layouts.app')
@section('content')
@include('header')
<section class="text-breadcrumb">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <ul>
               <li><a href="#">Our Products</a></li>
               <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
               <li><a href="#">Insecticide</a></li>
               <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
               <li><a href="#"><b>Monalisa 333SC </b></a></li>
            </ul>
         </div>
      </div>
   </div>
</section>
<section class="product-detial">
   <div class="container">
      <div class="row">
         <div class="col-lg-9">
            <div class="row">
               <div class="col-lg-4">
                  <div class="product-detial-img-box">
                     <img src="/assets/img/products/1.png" alt="">
                  </div>
               </div>
               <div class="col-lg-8">
                  <div class="product-info">
                     <h2>Monalisa 333SC</h2>
                     <h6>Abamectin 3.3% + Imidacloropid 30%</h6>
                     <p>A new generation insecticide &amp;/or Acaricide with contact and stomach action effective against sucking pest in field crops.</p>
                  </div>
               </div>
            </div>
            <div class="product-info">
               <div class="mt-4 pt-2">
                  <div class="table-responsive">
                     <table class="table table-bordered">
                        <thead>
                           <tr>
                              <th scope="col">Features</th>
                              <th scope="col">Benefits</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <th scope="row"> Broad spectrum of activity</th>
                              <td> Keefun is highly effective against a broad range of sucking pests (viz, jassids, thrips, aphids) and chewing and biting pests (Diamond Back Moth or DBM). Thus keefun works as one shot solution for more than one target pests also reducing the cost of crop protection.</td>
                           </tr>
                           <tr>
                              <th scope="row">  Different Mode of Action</th>
                              <td> Highly effective on insects resistant to the other existing insecticides.</td>
                           </tr>
                           <tr>
                              <th scope="row">  Anti-feedent action</th>
                              <td> Due to its anti-feedent action, target pests' stops feeding immediately after coming in contact with Keefun.</td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
               <div class="content mt-3">
                  <h5 class="sub-heading mb-0">  Mode of Action </h5>
                  <p> Keefun is a METI (Mitochondrial Electron Transfer Inhibitor) compound which acts through contact and ingestion. Keefun inhibits the electron transport chain in the mitochondria of the cell, leading to the cessation of the production and supply of the energy in the cell, resulting in the death of the target pest.</p>
               </div>
               <div class="content mt-3">
                  <h5 class="sub-heading mb-0">  Method of application </h5>
                  <p>  For effective results, Keefun should be used at the initial stage of the crop and insect. Ensure thorough and uniform coverage by using hollow cone nozzle. Rotate with insecticide of different mode of action to avoid resistance development. Avoid spray of keefun if the rains are expected earlier than 6 hours.</p>
               </div>
            </div>
         </div>
         <div class="col-lg-3 ">
            <div class="product-listing-box">
               <h4 class="sub-heading">Related Products</h4>
               <ul>
                  <li> <a href="javascript:;">Megacyper 250 EC</a></li>
                  <li> <a href="javascript:;">Pastifat 75SP</a></li>
                  <li> <a href="javascript:;">Megacyper 250 EC</a></li>
                  <li> <a href="javascript:;">ZESBAN 480 EC</a></li>
                  <li> <a href="javascript:;">Monalisa 333SC</a></li>
                  <li> <a href="javascript:;">Pastifat 75SP</a></li>
                  <li> <a href="javascript:;">Megacyper 250 EC</a></li>
                  <li> <a href="javascript:;">ZESBAN 480 EC</a></li>
                  <li> <a href="javascript:;">Monalisa 333SC</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="newsletter-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-11 m-auto">
            <div class="subscribe-box mt-0 p-0">
               <h4>Subscribe to get the latest updates</h4>
               <div class="subscribe-form">
                  <div class="row">
                     <div class="col-lg-8 col-sm-8 pl-sm-5">
                        <form>
                           <div class="form-group">
                              <input type="email" class="form-control" placeholder="Email">
                           </div>
                        </form>
                     </div>
                     <div class="pl-0 col-lg-4 col-sm-4">
                        <button type="submit" class="btn btn-green">Submit</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="last-section">
   <div class="footer-bg-color">
      <div class="container">
         <div class="footer-section">
            <div class="row">
               <div class="col-lg-3 col-sm-6">
                  <div class="footer-logo">
                     <img src="/assets/img/main-logo.png" alt="">
                  </div>
                  <p>Indoin is committed to Deliver Real Value to Customers in the field of Agrochemicals and ecofriendly bio products, by changing perception of Brand from High Price to High Value.</p>
                  <div class="social-icons">
                     <ul>
                        <li class="pl-0">
                           <a target="_blank" href="https://www.facebook.com/Indoin-Business-Group-600689813415378/"><img src="/assets/img/facebook-color.png" alt=""></a>
                        </li>
                        <li>
                           <a target="_blank" href="https://www.linkedin.com/company/indoin-business-group/"><img src="/assets/img/linkedin-color.png" alt=""></a>
                        </li>
                        <li>
                           <a target="_blank" href="https://twitter.com/GroupIndoin"><img src="/assets/img/twitter-color.png" alt=""></a>
                        </li>
                        <li>
                           <a target="_blank" href="https://www.instagram.com/indoinbusinessgroup/?hl=id"><img src="/assets/img/instagram-color.png" alt=""></a>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class=" col-lg-3 col-sm-6">
                  <h6>Head Office</h6>
                  <div class="footer-info">
                     <p>    <img src="/assets/img/loaction-icon.png" alt=""> <span>PT Indoin Business Group</span>
                        <br> <span class="pl-lg-3 pl-0">Palma One Building</span>
                        <br> <span  class="pl-lg-3 pl-0">2nd Floor - Suite 205</span>
                        <br> <span  class="pl-lg-3 pl-0">JL.Hr. Rasuna Said Kav. C2 No. 4</span>
                        <br> <span  class="pl-lg-3 pl-0">Jakarta, Indonesia 12950</span>
                     </p>
                     <a href="javascript:;">   <img src="/assets/img/phone-icon.png" alt="">   +62.21.522.8322</a>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <h6>India Office</h6>
                  <div class="footer-info">
                     <p>   <img src="/assets/img/loaction-icon.png" alt="">
                        <span> Indoin Biolife Pvt. Ltd.</span>
                        <br> <span class="pl-lg-3 pl-0">308-309, Shahpuri Tirath Singh </span>
                        <br> <span class="pl-lg-3 pl-0">TowerC-58, Janakpuri, Pin: 110 058</span>
                        <br> <span class="pl-lg-3 pl-0">New Delhi, India</span>
                     </p>
                     <a href="javascript:;">   <img src="/assets/img/phone-icon.png" alt=""> +91 11 4300 4612</a>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <h6>Production Unit</h6>
                  <div class="footer-info">
                     <p>   <img src="/assets/img/loaction-icon.png" alt="">
                        <span> Indoin Biolife Pvt. Ltd.</span>
                        <br> <span class="pl-lg-3 pl-0">Talod Road, Dhansura, Taluka  </span>
                        <br> <span class="pl-lg-3 pl-0">Dhansura, Dist Aravalli</span>
                        <br> <span class="pl-lg-3 pl-0">Gujarat, India -383310</span>
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="footer-bottom">
         <div class="container">
            <div class="footer-links">
               <div class="row">
                  <div class="col-lg-12 text-center">
                     <ul>
                        <li>© 2020 Indoin</li>
                        <li>
                           <a href="#">Privacy policy</a>
                        </li>
                        <li>
                           <a href="#">Term of service</a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
