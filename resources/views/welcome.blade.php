@extends('layouts.app')
@section('content')
@include('header')
<section class="banner-section">
   <div class="owl-carousel owl-theme homepage-banner">
      <div class="item">
         <div class="position-relative">
            <div class="slider-video background-overlay d-flex align-items-center">
               <video autoplay muted loop id="myVideo-1">
                  <source src="/assets/videos/video3.mp4" type="video/mp4">
               </video>
               <div class="container">
                  <div class="banner-content">
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="text-center">
                              <h5 class="sub-heading text-lowercase font-italic">Adding Value, Building Trust</h5>
                              <h1 class="main-heading">Kareri Mountain tour & Travel</h1>
                           </div>
                        </div>
                        <div class="col-lg-9 m-auto">
                           <div class="text-center">
                              <p class="simple-text">Indoin is committed to Deliver Real Value to Customers, by providing agricultural solutions in the form of plant protection and plant nutrition and changing the perception of Brand from High Price to High Value.
                              </p>
                              <a class="call-to-action" href="/assets/about-us.html">Learn More</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="item">
         <div class="position-relative">
            <div class="slider-video background-overlay d-flex align-items-center">
               <video autoplay muted loop id="myVideo">
                  <source src="/assets/videos/video2.mp4" type="video/mp4">
               </video>
               <div class="container">
                  <div class="banner-content">
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="text-center">
                              <h5 class="sub-heading text-lowercase font-italic">Adding Value, Building Trust</h5>
                              <h1 class="main-heading">Kareri Mountain tour & Travel</h1>
                           </div>
                        </div>
                        <div class="col-lg-9 m-auto">
                           <div class="text-center">
                              <p class="simple-text">Indoin is committed to Deliver Real Value to Customers, by providing agricultural solutions in the form of plant protection and plant nutrition and changing the perception of Brand from High Price to High Value.
                              </p>
                              <a class="call-to-action" href="/our-story">Learn More</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   </div>
</section>
<section class="about-us-new-section position-relative">
   <div class="social-media-side">
      <ul>
         <li><a target="_blank" href="https://www.facebook.com/Indoin-Business-Group-600689813415378/"><img src="/assets/img/facebook-color.png" alt=""></a></li>
         <li><a target="_blank" href="https://www.linkedin.com/company/indoin-business-group/"><img src="/assets/img/linkedin-color.png" alt=""></a></li>
         <li><a target="_blank" href="https://twitter.com/GroupIndoin"><img src="/assets/img/twitter-color.png" alt=""></a></li>
         <li><a target="_blank" href="https://www.instagram.com/indoinbusinessgroup/?hl=id"><img class="mb-0" src="/assets/img/instagram-color.png" alt=""></a></li>
      </ul>
   </div>
   <div class="container">
      <div class="row">
         <div class="col-lg-5 ">
            <h5 class="sub-heading">Welcome to Kareri Mountain tour & Travel</h5>
            <h1 class="green-main-heading mt-3">About <span>Us</span></h1>
            <p class="mt-3 "><b>IN ‘DO’ IN</b> believe each <b>IN</b>dividual
               can <b>DO</b> <b>IN</b>finite things.  Core philosophy
               of the  company lies in realizing true  potential
               of  Individual partners in delivering value.  This
               is joint effort by all levels of value chain
               providing  best solutions  to the customers. It
               is  the right of the customer to get best value
               for its spend and its our responsibility
               and  commitment to deliver the same.
            </p>
         </div>
         <div class="col-lg-6 offset-lg-1">
            <div class="hover-abt-box hb-1">
               <img src="/assets/images/logo-1.png" alt="">
               <a href="javascript:;">
                  <h4>PT Indoin Business Group </h4>
               </a>
               <p class="hover-hide-1">As early as 5 years back, a very humble start from Indonesia with limited product range and resources, Group has registered multiple fold growth of business, product portfolio and team members since inception. Currently one of the most aggressive and fastest growing company of Indonesia INDOIN has its presence across country. The phenomenal success and expansion is result of unique marketing strategies, dedicated team, trust of business partners and the VALUE company have created for its end customers. The journey is progressing even more exciting with multiple introductions of unique and latest off patented products in the portfolio. We are ready for ADDING VALUE BUILDING TRUST across value chain .</p>
            </div>
            <div class="hover-abt-box hb-2">
               <img src="/assets/img/logo-2.png" alt="">
               <a href="javascript:;">
                  <h4>PT Masco Agri Genetics </h4>
               </a>
               <p class="hover-hide-2 ">In our quest to provide complete Agri input solutions to the farmers, we moved one step ahead in the year 2017 and introduced our Seed Portfolio with Corn Hybrids which became instant acceptable at Farmer level right from first year. Year 2019-20 have witnessed successful Corn Hybrid local seed production results under our close supervision. The production volumes are already scaling up multi fold right from next season. For last 3 years, dedicated product development team across islands of Indonesia working round the year, are testing and screening best fit vegetable Hybrids sourced from top breeding companies and universities across globe. The hard and efficient work have led to selection of high yielding and most adaptable vegetable seed range for multiple crops. The company is ready to sow its SEEDS OF SUCCESS. </p>
            </div>
            <div class="hover-abt-box hb-3">
               <img src="/assets/img/logo-1.png" alt="">
               <a href="about-us-india.html">
                  <h4>Indoin Biolife Pvt. Ltd </h4>
               </a>
               <p class="hover-hide-3">India as home ground was always a natural first choice for market expansion and the footprint is the proud start of our journey of aspiring Global player. Introduction of Bio Fertilizers and Nutrition range followed by Agrochemicals in a very short span of time is result of dedicated team and strong system processes developed in last 5 years. Quick expansion of business network across North India states have set right stage for next level expansion plan in almost 12 Indian states in couple of months. In house production for multiple CIB registrations and Tie ups with partner companies for various unique products powered with strong marketing team is ready to create visible market presence. We are ready for ADDING VALUE BUILDING TRUST across value chain.</p>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="youtube-video-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-6">
            <div class="video-thumb">
               <iframe width="100%" height="375px" src="https://www.youtube.com/embed/5TkeN8NHYU8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
         </div>
         <div class="col-lg-6">
            <div class="video-content">
               <div class="">
                  <h1>Spirit of Indoin</h1>
                  <p>Celebrate the diversity of culture, people, <br> traditions, crops and agribusiness with Sprit of <br> Passion, Intergrity, Value Creation, Ownership, <br> Trust and Team Work.</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="isotope-gallery-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-4 col-4 col-sm-4">
            <div class="isotope-img">
               <a class="isotope-overlay-440"  href="happenings.html#indoin-ac">
                  <img class="h-440 " src="/assets/images/image1.jpeg" alt="">
                  <h6>Anniversary And Celebration</h6>
               </a>
            </div>
            <div class="isotope-img">
               <a class="isotope-overlay-310"  href="happenings.html#indoin-ae">
                  <img class="h-310" src="/assets/images/image2.jpeg" alt="">
                  <h6>
                     Awards And Events
                  </h6>
               </a>
            </div>
         </div>
         <div class="col-lg-4 col-4 col-sm-4">
            <div class="isotope-img">
               <a class="isotope-overlay-375"  href="happenings.html#indoin-ft">
                  <img class="h-375" src="/assets/images/image3.jpeg" alt="">
                  <h6>
                     Family and Trip
                  </h6>
               </a>
            </div>
            <div class="isotope-img">
               <a class="isotope-overlay-375"  href="happenings.html#indoin-fm">
                  <img class="h-375" src="/assets/images/image4.jpeg" alt="">
                  <h6>
                     Field Meetings
                  </h6>
               </a>
            </div>
         </div>
         <div class="col-lg-4 col-4 col-sm-4">
            <div class="isotope-img">
               <a class="isotope-overlay-240"  href="happenings.html#indoin-ic">
                  <img class="h-240" src="/assets/images/image5.jpeg" alt="">
                  <h6>
                     Indoin Care
                  </h6>
               </a>
            </div>
            <div class="isotope-img">
               <a class="isotope-overlay-240" href="happenings.html#indoin-ac">
                  <img class="h-240" src="/assets/images/image6.jpeg" alt="">
                  <h6>Anniversary And Celebration</h6>
               </a>
               </a>
            </div>
            <div class="isotope-img">
               <a  class="isotope-overlay-240" href="happenings.html#indoin-ft">
                  <img class="h-240" src="/assets/images/image7.jpeg" alt="">
                  <h6>
                     Family and Trip
                  </h6>
               </a>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="ceo-ach-section">
   <div class="container-fluid">
      <div class="row">
         <div class="col-lg-6 pl-0 pr-0">
            <div class="ceo-words">
               <h3>Words from our CEO</h3>
               <p>Despite a consistent increase of farm output in last few decades,
                  the growth from farm income is still not in pace with other sectors
                  which have progressed   multifold.   Agri   input   industry   with
                  affordable   solutions combined   with   new   technology   could
                  be   leading   factor   to   bring  big change. This is the time
                  for Farmers to get the REAL VALUE and INDOIN is committed to bring
                  REAL CHANGE they deserve.
               </p>
               <p class="pt-4 mb-0"><b>Pradeep Bahuguna</b></p>
               <span>Founder and Global CEO,  INDOIN BUSINESS GROUP</span>
            </div>
         </div>
         <div class="col-lg-6 pl-0 pr-0">
            <div class="ach-section">
               <div class="row">
                  <div class="col-lg-8">
                     <h3>Achievements</h3>
                  </div>
                  <div class="col-lg-4">
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-12">
                     <div class="owl-carousel owl-theme ach-section-owl">
                        <div class="item">
                           <div class="achievement-box">
                              <div class="">
                                 <img src="/assets/images/image8.jpeg" alt="">
                                 <h5>Fastest growing <br> Agchem company of <br> Indonesia</h5>
                                 <p>INDONESIA</p>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="achievement-box">
                              <div class="">
                                 <img src="/assets/images/image9.jpeg" alt="">
                                 <h5>Best Brand Award <br> 2018 by CMO, Asia</h5>
                                 <p>INDONESIA</p>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="achievement-box">
                              <div class="">
                                 <img src="/assets/images/image10.jpeg" alt="">
                                 <h5>Network of 5000 plus <br> retailers across <br> Indonesia</h5>
                                 <p>INDONESIA</p>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="achievement-box">
                              <div class="">
                                 <img src="/assets/images/image12.jpeg" alt="">
                                 <h5>Acquisitions of 33 <br> brands and <br> registrations.</h5>
                                 <p>INDONESIA</p>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="achievement-box">
                              <div class="">
                                 <img src="/assets/images/image13.jpeg" alt="">
                                 <h5>Most innovative <br> product portfolio</h5>
                                 <p>INDONESIA</p>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="achievement-box">
                              <div class="">
                                 <img src="/assets/images/image14.jpeg" alt="">
                                 <h5>Key player in Palm <br> Plantation Insect <br> control.</h5>
                                 <p>INDONESIA</p>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="achievement-box">
                              <div class="">
                                 <img src="/assets/images/image15.jpeg" alt="">
                                 <h5>Entry into local seed <br> production of corn.</h5>
                                 <p>INDONESIA</p>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="achievement-box">
                              <div class="">
                                 <img src="/assets/images/image16.jpeg" alt="">
                                 <h5>Leader in Blue <br> Mancozeb segment</h5>
                                 <p>INDONESIA</p>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="achievement-box">
                              <div class="">
                                 <img src="/assets/images/image17.jpeg" alt="">
                                 <h5>Key player  <br>  in Cooperative <br> society  supplies <br>  in North India.</h5>
                                 <p>INDIA</p>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="achievement-box">
                              <div class="">
                                 <img src="/assets/images/image18.jpeg" alt="">
                                 <h5>MOU with <br> Uttarakhand Govn. <br> For Bio Fertilities Unit.</h5>
                                 <p>INDIA</p>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="achievement-box">
                              <div class="">
                                 <img src="/assets/images/image19.jpeg" alt="">
                                 <h5>Commissioning of <br> Agchem production <br> unit in India</h5>
                                 <p>INDIA</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="newsletter-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-11 m-auto">
            <div class="subscribe-box mt-0 p-0">
               <h4>Subscribe to get the latest updates</h4>
               <div class="subscribe-form">
                  <div class="row">
                     <div class="col-lg-8 col-sm-8 pl-sm-5">
                        <form>
                           <div class="form-group">
                              <input type="email" class="form-control" placeholder="Email">
                           </div>
                        </form>
                     </div>
                     <div class="pl-0 col-lg-4 col-sm-4">
                        <button type="submit" class="btn btn-green">Submit</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="last-section">
   <div class="footer-bg-color">
      <div class="container">
         <div class="footer-section">
            <div class="row">
               <div class="col-lg-3 col-sm-6">
                  <div class="footer-logo">
                     <img src="/assets/img/main-logo.png" alt="">
                  </div>
                  <p>Indoin is committed to Deliver Real Value to Customers in the field of Agrochemicals and ecofriendly bio products, by changing perception of Brand from High Price to High Value.</p>
                  <div class="social-icons">
                     <ul>
                        <li class="pl-0">
                           <a target="_blank" href="https://www.facebook.com/Indoin-Business-Group-600689813415378/"><img src="/assets/img/facebook-color.png" alt=""></a>
                        </li>
                        <li>
                           <a target="_blank" href="https://www.linkedin.com/company/indoin-business-group/"><img src="/assets/img/linkedin-color.png" alt=""></a>
                        </li>
                        <li>
                           <a target="_blank" href="https://twitter.com/GroupIndoin"><img src="/assets/img/twitter-color.png" alt=""></a>
                        </li>
                        <li>
                           <a target="_blank" href="https://www.instagram.com/indoinbusinessgroup/?hl=id"><img src="/assets/img/instagram-color.png" alt=""></a>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class=" col-lg-3 col-sm-6">
                  <h6>Head Office</h6>
                  <div class="footer-info">
                     <p>    <img src="/assets/img/loaction-icon.png" alt=""> <span>PT Indoin Business Group</span>
                        <br> <span class="pl-lg-3 pl-0">Palma One Building</span>
                        <br> <span  class="pl-lg-3 pl-0">2nd Floor - Suite 205</span>
                        <br> <span  class="pl-lg-3 pl-0">JL.Hr. Rasuna Said Kav. C2 No. 4</span>
                        <br> <span  class="pl-lg-3 pl-0">Jakarta, Indonesia 12950</span>
                     </p>
                     <a href="javascript:;">   <img src="/assets/img/phone-icon.png" alt="">   +62.21.522.8322</a>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <h6>India Office</h6>
                  <div class="footer-info">
                     <p>   <img src="/assets/img/loaction-icon.png" alt="">
                        <span> Indoin Biolife Pvt. Ltd.</span>
                        <br> <span class="pl-lg-3 pl-0">308-309, Shahpuri Tirath Singh </span>
                        <br> <span class="pl-lg-3 pl-0">TowerC-58, Janakpuri, Pin: 110 058</span>
                        <br> <span class="pl-lg-3 pl-0">New Delhi, India</span>
                     </p>
                     <a href="javascript:;">   <img src="/assets/img/phone-icon.png" alt=""> +91 11 4300 4612</a>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <h6>Production Unit</h6>
                  <div class="footer-info">
                     <p>   <img src="/assets/img/loaction-icon.png" alt="">
                        <span> Indoin Biolife Pvt. Ltd.</span>
                        <br> <span class="pl-lg-3 pl-0">Talod Road, Dhansura, Taluka  </span>
                        <br> <span class="pl-lg-3 pl-0">Dhansura, Dist Aravalli</span>
                        <br> <span class="pl-lg-3 pl-0">Gujarat, India -383310</span>
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="footer-bottom">
         <div class="container">
            <div class="footer-links">
               <div class="row">
                  <div class="col-lg-12 text-center">
                     <ul>
                        <li>© 2020 Indoin</li>
                        <li>
                           <a href="#">Privacy policy</a>
                        </li>
                        <li>
                           <a href="#">Term of service</a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
