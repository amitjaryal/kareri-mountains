@extends('layouts.app')
@section('content')
@include('header')
<section class="divider-section pt-0 pb-0">
   <div class="divider-team-image">
      <img src="/assets/img/team-bg.jpeg" alt="">
   </div>
</section>

<section class="about-logo-desc">
   <div class="container">
      <div class="row">
         <div class="col-lg-6">
            <div class="about-logo-left">
               <h1 class="main-small-heading" style="color:#99CF54;">About Our
                  <span style="color:#007044;">Logo</span>
               </h1>
               <div class="content">
                  <p>Each alphabet in Pictorial logo of INDOIN is symbolic to our philosophy. </p>
                  <p>Green color of IndoIn symbolizes prosperity, health, value and nature.</p>
                  <p>The tricolor leafs separated by white background at the start of logo represent flag color of Indonesia and India from where the name INDOIN is derived. The three colors represent Passion, energy and new hope.</p>
               </div>
            </div>
         </div>
         <div class="col-lg-6">
            <div class="logo-define">
               <img src="/assets/img/logo-define.png" alt="">
            </div>
         </div>
      </div>
   </div>
</section>
<section class="real-value-section pt-0 pb-0" style="background:url('/assets/img/rv-bg.jpg') fixed;">
   <div class="real-value-overlay">
      <div class="container">
         <div class="row">
            <div class="col-lg-4">
               <div class="real-brand-logo-about">
                  <img src="/assets/img/real-value-img.png" alt="">
                  <h1 class="main-small-heading" style="color:#fff !important;">Real Value <br> Brand</h1>
               </div>
            </div>
            <div class="col-lg-8">
               <div class="real-content-center">
                  <div class="content pt-2">
                     <h5 class="sub-heading mb-0" style="color:#fff !important;">Learn About Real Value Brand </h5>
                     <p style="color:#fff !important;">Brand which stands on the existence of fact, not imagined or supposed and proves to be something it held to deserve in importance and usefulness.
                     </p>
                     <p style="color:#fff !important;">FALSE VALUE build on customers ignorance and hyped branding will wipe with customers awareness and REAL VALUE experience.</p>
                     <p style="color:#fff !important;">INDOIN with its REAL VALUE BRANDS is committed to provide useful and economical farm solutions for sustainable Agriculture.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="know-future mt-0" id="vision-mission">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <h1 class="main-small-heading">Get to Know Our Future</h1>
         </div>
      </div>
      <div class="row pt-4 mt-2">
         <div class="col-lg-6">
            <div class="vision-box">
               <div class="row">
                  <div class="icon-box">
                     <img src="/assets/img/vision-icon.png" alt="">
                  </div>
                  <div class="col-lg-6">
                     <h3>Vision</h3>
                  </div>
               </div>
               <div class="content">
                  <p style="color:#fff;">To become worlds most valued company by <b>DELIVERING REAL VALUE</b> to our customers. </p>
                  <p style="color:#fff;">  Build most trusted association with our partners, customers and team members.</p>
               </div>
            </div>
         </div>
         <div class="col-lg-6">
            <div class="vision-box mission-box">
               <div class="row">
                  <div class="icon-box">
                     <img src="/assets/img/mission-icon.png" alt="">
                  </div>
                  <div class="col-lg-6">
                     <h3>Mission</h3>
                  </div>
               </div>
               <div class="content">
                  <p style="color:#fff;">To Change perception of brand from <b>HIGH PRICE TO HIGH VALUE</b>.</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="our-values-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 pb-lg-4">
            <h1 class="main-small-heading">Our Values</h1>
         </div>
      </div>
      <div class="row">
         <div class="col-lg-12 m-auto">
            <div class="row">
               <div class="col-lg-12">
                  <div class="pivot-text">
                     <ul>
                        <li>
                           <h1 style="background:#a2d45e;">P</h1>
                        </li>
                        <li>
                           <h1 style="background:#f88d2a;">i</h1>
                        </li>
                        <li>
                           <h1 style="background:#007a4c;">v</h1>
                        </li>
                        <li>
                           <h1 style="background:#e1251b;">o</h1>
                        </li>
                        <li>
                           <h1 style="background:#a2d45e;">t</h1>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-12">
                  <div class="pivot-text pt-0">
                     <ul class="pivot-btn-ul mb-0">
                        <li>
                           <button type="button" class="btn btn-values" name="button">Passion</button>
                           <p class="d-block d-lg-none mb-0">We Are Passionate About Our Company, Our Brands And Our Values.</p>
                        </li>
                        <li>
                           <button type="button" class="btn btn-values" name="button">Intergrity</button>
                           <p class="d-block d-lg-none mb-0">We Will Be Acting With Absolute Intergrity.</p>
                        </li>
                        <li>
                           <button type="button" class="btn btn-values" name="button">Value Creation</button>
                           <p class="d-block d-lg-none mb-0">We Will Build And Share Real Value.</p>
                        </li>
                        <li>
                           <button type="button" class="btn btn-values" name="button">Ownership</button>
                           <p class="d-block d-lg-none mb-0">We Will Do What Need To Be Done</p>
                        </li>
                        <li>
                           <button type="button" class="btn btn-values" name="button">Trust & Teamwork</button>
                           <p class="d-block d-lg-none mb-0">We Recognise That We Are Stronger And More Effective As A Team Than As Individuals.</p>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-12 d-none d-lg-block">
                  <div class="pivot-desc-text pt-0">
                     <ul>
                        <li>
                           <p>We Are Passionate About Our Company, Our Brands And Our Values.</p>
                        </li>
                        <li>
                           <p>We Will Be Acting With Absolute Intergrity.</p>
                        </li>
                        <li>
                           <p>We Will Build And Share Real Value.</p>
                        </li>
                        <li>
                           <p>We Will Do What Need To Be Done.</p>
                        </li>
                        <li>
                           <p>We Recognise That We Are Stronger And More Effective As A Team Than As Individuals.</p>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="newsletter-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-11 m-auto">
            <div class="subscribe-box mt-0 p-0">
               <h4>Subscribe to get the latest updates</h4>
               <div class="subscribe-form">
                  <div class="row">
                     <div class="col-lg-8 col-sm-8 pl-sm-5">
                        <form>
                           <div class="form-group">
                              <input type="email" class="form-control" placeholder="Email">
                           </div>
                        </form>
                     </div>
                     <div class="pl-0 col-lg-4 col-sm-4">
                        <button type="submit" class="btn btn-green">Submit</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="last-section">
   <div class="footer-bg-color">
      <div class="container">
         <div class="footer-section">
            <div class="row">
               <div class="col-lg-3 col-sm-6">
                  <div class="footer-logo">
                     <img src="/assets/img/main-logo.png" alt="">
                  </div>
                  <p>Indoin is committed to Deliver Real Value to Customers in the field of Agrochemicals and ecofriendly bio products, by changing perception of Brand from High Price to High Value.</p>
                  <div class="social-icons">
                     <ul>
                        <li class="pl-0">
                           <a target="_blank" href="https://www.facebook.com/Indoin-Business-Group-600689813415378/"><img src="/assets/img/facebook-color.png" alt=""></a>
                        </li>
                        <li>
                           <a target="_blank" href="https://www.linkedin.com/company/indoin-business-group/"><img src="/assets/img/linkedin-color.png" alt=""></a>
                        </li>
                        <li>
                           <a target="_blank" href="https://twitter.com/GroupIndoin"><img src="/assets/img/twitter-color.png" alt=""></a>
                        </li>
                        <li>
                           <a target="_blank" href="https://www.instagram.com/indoinbusinessgroup/?hl=id"><img src="/assets/img/instagram-color.png" alt=""></a>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class=" col-lg-3 col-sm-6">
                  <h6>Head Office</h6>
                  <div class="footer-info">
                     <p>    <img src="/assets/img/loaction-icon.png" alt=""> <span>PT Indoin Business Group</span>
                        <br> <span class="pl-lg-3 pl-0">Palma One Building</span>
                        <br> <span  class="pl-lg-3 pl-0">2nd Floor - Suite 205</span>
                        <br> <span  class="pl-lg-3 pl-0">JL.Hr. Rasuna Said Kav. C2 No. 4</span>
                        <br> <span  class="pl-lg-3 pl-0">Jakarta, Indonesia 12950</span>
                     </p>
                     <a href="javascript:;">   <img src="/assets/img/phone-icon.png" alt="">   +62.21.522.8322</a>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <h6>India Office</h6>
                  <div class="footer-info">
                     <p>   <img src="/assets/img/loaction-icon.png" alt="">
                        <span> Indoin Biolife Pvt. Ltd.</span>
                        <br> <span class="pl-lg-3 pl-0">308-309, Shahpuri Tirath Singh </span>
                        <br> <span class="pl-lg-3 pl-0">TowerC-58, Janakpuri, Pin: 110 058</span>
                        <br> <span class="pl-lg-3 pl-0">New Delhi, India</span>
                     </p>
                     <a href="javascript:;">   <img src="/assets/img/phone-icon.png" alt=""> +91 11 4300 4612</a>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <h6>Production Unit</h6>
                  <div class="footer-info">
                     <p>   <img src="/assets/img/loaction-icon.png" alt="">
                        <span> Indoin Biolife Pvt. Ltd.</span>
                        <br> <span class="pl-lg-3 pl-0">Talod Road, Dhansura, Taluka  </span>
                        <br> <span class="pl-lg-3 pl-0">Dhansura, Dist Aravalli</span>
                        <br> <span class="pl-lg-3 pl-0">Gujarat, India -383310</span>
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="footer-bottom">
         <div class="container">
            <div class="footer-links">
               <div class="row">
                  <div class="col-lg-12 text-center">
                     <ul>
                        <li>© 2020 Indoin</li>
                        <li>
                           <a href="#">Privacy policy</a>
                        </li>
                        <li>
                           <a href="#">Term of service</a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
