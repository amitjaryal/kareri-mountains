@extends('layouts.app')
@section('content')
@include('header')
<section class="products-table-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-8">
            <h1 class="main-small-heading mb-0">Products</h1>
            <ul class="product-btn-list pt-3 pb-lg-3 pb-0 mb-0 mb-lg-3">
               <li>
                  <button type="button" class="btn btn-orange" name="button">Insecticide</button>
               </li>
               <li>
                  <button type="button" class="btn btn-light-green" name="button">Fungicide</button>
               </li>
               <li>
                  <button type="button" class="btn btn-red" name="button">Herbicide</button>
               </li>
               <li>
                  <button type="button" class="btn btn-dark-green" name="button">Seeds</button>
               </li>
            </ul>
            <div class="search-field  d-block d-lg-none">
               <form>
                  <div class="form-group">
                     <input type="text" class="form-control" placeholder="Search">
                     <img class="" src="/assets/img/search-icon.png" alt="">
                  </div>
               </form>
            </div>
            <div class="Insecticide">
               <div class="table-responsive">
                  <table class="table table-hover">
                     <thead>
                        <tr>
                           <th scope="col">Product Name</th>
                           <th scope="col">Salt In</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-new-icon.png" alt="">
                              <a href="/package/1">Monalisa 333SC</a>
                           </td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-trending-icon.png" alt="">
                              <a href="/package/1">Megacyper 250 EC</a>
                           </td>
                           <td>Cypermethrin 25%</td>
                        </tr>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-star-icon.png" alt="">
                              <a href="/package/1">Pastifat 75SP</a>
                           </td>
                           <td>Acephate 75%</td>
                        </tr>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-trending-icon.png" alt="">
                              <img class="bs-icon" src="/assets/img/pr-new-icon.png" alt="">
                              <a href="/package/1">ZESBAN 480 EC</a>
                           </td>
                           <td>Chlorpyrphos 480 g/l</td>
                        </tr>
                        <tr>
                           <td><a href="/package/1">Monalisa 333SC</a></td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">Monalisa 333SC</a>
                           </td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">Megacyper 250 EC</a>
                           </td>
                           <td>Cypermethrin 25%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">Pastifat 75SP</a>
                           </td>
                           <td>Acephate 75%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">ZESBAN 480 EC</a>
                           </td>
                           <td>Chlorpyrphos 480 g/l</td>
                        </tr>
                        <tr>
                           <td><a href="/package/1">Monalisa 333SC</a></td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
            <button type="button" class="btn btn-light-green mb-4" name="button">Fungicide</button>
            <div class="Fungicide">
               <div class="table-responsive">
                  <table class="table table-hover">
                     <thead>
                        <tr>
                           <th scope="col">Product Name</th>
                           <th scope="col">Salt In</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-new-icon.png" alt="">
                              <a href="/package/1">Monalisa 333SC</a>
                           </td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-trending-icon.png" alt="">
                              <a href="/package/1">Megacyper 250 EC</a>
                           </td>
                           <td>Cypermethrin 25%</td>
                        </tr>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-star-icon.png" alt="">
                              <a href="/package/1">Pastifat 75SP</a>
                           </td>
                           <td>Acephate 75%</td>
                        </tr>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-trending-icon.png" alt="">
                              <img class="bs-icon" src="/assets/img/pr-new-icon.png" alt="">
                              <a href="/package/1">ZESBAN 480 EC</a>
                           </td>
                           <td>Chlorpyrphos 480 g/l</td>
                        </tr>
                        <tr>
                           <td><a href="/package/1">Monalisa 333SC</a></td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">Monalisa 333SC</a>
                           </td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">Megacyper 250 EC</a>
                           </td>
                           <td>Cypermethrin 25%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">Pastifat 75SP</a>
                           </td>
                           <td>Acephate 75%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">ZESBAN 480 EC</a>
                           </td>
                           <td>Chlorpyrphos 480 g/l</td>
                        </tr>
                        <tr>
                           <td><a href="/package/1">Monalisa 333SC</a></td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
            <button type="button" class="btn btn-red mb-3" name="button">Herbicide</button>
            <div class="Herbicide">
               <div class="table-responsive">
                  <table class="table table-hover">
                     <thead>
                        <tr>
                           <th scope="col">Product Name</th>
                           <th scope="col">Salt In</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-new-icon.png" alt="">
                              <a href="/package/1">Monalisa 333SC</a>
                           </td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-trending-icon.png" alt="">
                              <a href="/package/1">Megacyper 250 EC</a>
                           </td>
                           <td>Cypermethrin 25%</td>
                        </tr>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-star-icon.png" alt="">
                              <a href="/package/1">Pastifat 75SP</a>
                           </td>
                           <td>Acephate 75%</td>
                        </tr>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-trending-icon.png" alt="">
                              <img class="bs-icon" src="/assets/img/pr-new-icon.png" alt="">
                              <a href="/package/1">ZESBAN 480 EC</a>
                           </td>
                           <td>Chlorpyrphos 480 g/l</td>
                        </tr>
                        <tr>
                           <td><a href="/package/1">Monalisa 333SC</a></td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">Monalisa 333SC</a>
                           </td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">Megacyper 250 EC</a>
                           </td>
                           <td>Cypermethrin 25%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">Pastifat 75SP</a>
                           </td>
                           <td>Acephate 75%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">ZESBAN 480 EC</a>
                           </td>
                           <td>Chlorpyrphos 480 g/l</td>
                        </tr>
                        <tr>
                           <td><a href="/package/1">Monalisa 333SC</a></td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
            <button type="button" class="btn btn-dark-green mb-3" name="button">Bio Products</button>
            <div class="Bio-Products">
               <div class="table-responsive">
                  <table class="table table-hover">
                     <thead>
                        <tr>
                           <th scope="col">Product Name</th>
                           <th scope="col">Salt In</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-new-icon.png" alt="">
                              <a href="/package/1">Monalisa 333SC</a>
                           </td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-trending-icon.png" alt="">
                              <a href="/package/1">Megacyper 250 EC</a>
                           </td>
                           <td>Cypermethrin 25%</td>
                        </tr>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-star-icon.png" alt="">
                              <a href="/package/1">Pastifat 75SP</a>
                           </td>
                           <td>Acephate 75%</td>
                        </tr>
                        <tr>
                           <td>
                              <img class="fp-icon" src="/assets/img/pr-trending-icon.png" alt="">
                              <img class="bs-icon" src="/assets/img/pr-new-icon.png" alt="">
                              <a href="/package/1">ZESBAN 480 EC</a>
                           </td>
                           <td>Chlorpyrphos 480 g/l</td>
                        </tr>
                        <tr>
                           <td><a href="/package/1">Monalisa 333SC</a></td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">Monalisa 333SC</a>
                           </td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">Megacyper 250 EC</a>
                           </td>
                           <td>Cypermethrin 25%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">Pastifat 75SP</a>
                           </td>
                           <td>Acephate 75%</td>
                        </tr>
                        <tr>
                           <td>
                              <a href="/package/1">ZESBAN 480 EC</a>
                           </td>
                           <td>Chlorpyrphos 480 g/l</td>
                        </tr>
                        <tr>
                           <td><a href="/package/1">Monalisa 333SC</a></td>
                           <td>Abamectin 3.3% + Imidacloropid 30%</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="col-lg-4 d-none d-lg-block">
            <div class="p-2 p-lg-5">
               <div class="search-field">
                  <form>
                     <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <img class="" src="/assets/img/search-icon.png" alt="">
                     </div>
                  </form>
               </div>
               <div class="">
                  <div class="tb-product-box">
                     <div class="product-box">
                        <div class="img-box">
                           <img src="/assets/img/products/1.png" alt="">
                        </div>
                     </div>
                     <div class="product-content">
                        <h3 class="product-name">Monalisa 333SC</h3>
                        <span class="cat-badge">Insecticide</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="last-section">
   <div class="footer-bg-color">
      <div class="container">
         <div class="footer-section">
            <div class="row">
               <div class="col-lg-3 col-sm-6">
                  <div class="footer-logo">
                     <img src="/assets/img/main-logo.png" alt="">
                  </div>
                  <p>Indoin is committed to Deliver Real Value to Customers in the field of Agrochemicals and ecofriendly bio products, by changing perception of Brand from High Price to High Value.</p>
                  <div class="social-icons">
                     <ul>
                        <li class="pl-0">
                           <a target="_blank" href="https://www.facebook.com/Indoin-Business-Group-600689813415378/"><img src="/assets/img/facebook-color.png" alt=""></a>
                        </li>
                        <li>
                           <a target="_blank" href="https://www.linkedin.com/company/indoin-business-group/"><img src="/assets/img/linkedin-color.png" alt=""></a>
                        </li>
                        <li>
                           <a target="_blank" href="https://twitter.com/GroupIndoin"><img src="/assets/img/twitter-color.png" alt=""></a>
                        </li>
                        <li>
                           <a target="_blank" href="https://www.instagram.com/indoinbusinessgroup/?hl=id"><img src="/assets/img/instagram-color.png" alt=""></a>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class=" col-lg-3 col-sm-6">
                  <h6>Head Office</h6>
                  <div class="footer-info">
                     <p>    <img src="/assets/img/loaction-icon.png" alt=""> <span>PT Indoin Business Group</span>
                        <br> <span class="pl-lg-3 pl-0">Palma One Building</span>
                        <br> <span  class="pl-lg-3 pl-0">2nd Floor - Suite 205</span>
                        <br> <span  class="pl-lg-3 pl-0">JL.Hr. Rasuna Said Kav. C2 No. 4</span>
                        <br> <span  class="pl-lg-3 pl-0">Jakarta, Indonesia 12950</span>
                     </p>
                     <a href="javascript:;">   <img src="/assets/img/phone-icon.png" alt="">   +62.21.522.8322</a>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <h6>India Office</h6>
                  <div class="footer-info">
                     <p>   <img src="/assets/img/loaction-icon.png" alt="">
                        <span> Indoin Biolife Pvt. Ltd.</span>
                        <br> <span class="pl-lg-3 pl-0">308-309, Shahpuri Tirath Singh </span>
                        <br> <span class="pl-lg-3 pl-0">TowerC-58, Janakpuri, Pin: 110 058</span>
                        <br> <span class="pl-lg-3 pl-0">New Delhi, India</span>
                     </p>
                     <a href="javascript:;">   <img src="/assets/img/phone-icon.png" alt=""> +91 11 4300 4612</a>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <h6>Production Unit</h6>
                  <div class="footer-info">
                     <p>   <img src="/assets/img/loaction-icon.png" alt="">
                        <span> Indoin Biolife Pvt. Ltd.</span>
                        <br> <span class="pl-lg-3 pl-0">Talod Road, Dhansura, Taluka  </span>
                        <br> <span class="pl-lg-3 pl-0">Dhansura, Dist Aravalli</span>
                        <br> <span class="pl-lg-3 pl-0">Gujarat, India -383310</span>
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="footer-bottom">
         <div class="container">
            <div class="footer-links">
               <div class="row">
                  <div class="col-lg-12 text-center">
                     <ul>
                        <li>© 2020 Indoin</li>
                        <li>
                           <a href="#">Privacy policy</a>
                        </li>
                        <li>
                           <a href="#">Term of service</a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>@endsection
