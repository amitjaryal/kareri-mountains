<!DOCTYPE html>
<html>
   <head>
      <title>Indoin | Home</title>
      <meta charset="UTF-8">
      <meta name="description" content="Indoin">
      <meta name="keywords" content="Indoin">
      <meta name="author" content="Indoin">
      <link rel="icon" href="/assets/img/ico.png" type="image/png">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="/assets/css/style.css">
      <link rel="stylesheet" href="/assets/css/responsive.css">
      <link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css">
      <link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.theme.default.min.css">
      <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
      <link rel="stylesheet" href="/assets/css/oka_slider_model.css" />
      <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
   </head>
   <body onload="setPlaySpeed()">
      @yield('content')

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js"></script>
      <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
      <script type="text/javascript" src="/assets/js/custom.js"></script>
      <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="/assets/js/oka_slider_model.js"></script>
      <script>
         $(document).ready(function(){
               $(".hb-1").hover(function(){
               $(".hover-hide-2").hide();
                 $(".hover-hide-3").hide();
               }, function(){
                 $(".hover-hide-2").show();
                   $(".hover-hide-3").show();
             });

               $(".hb-2").hover(function(){
               $(".hover-hide-1").hide();
                 $(".hover-hide-3").hide();
               }, function(){
                 $(".hover-hide-1").show();
                   $(".hover-hide-3").show();
             });

             $(".hb-3").hover(function(){
             $(".hover-hide-1").hide();
               $(".hover-hide-2").hide();
             }, function(){
               $(".hover-hide-1").show();
                 $(".hover-hide-2").show();
           });
         });
      </script>
   </body>
</html>
