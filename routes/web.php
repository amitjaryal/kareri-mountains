<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/our-story', function () {
    return view('/our-story');
});
Route::get('/packages', function () {
    return view('/packages');
});
Route::get('/package/{id}', function () {
    return view('/package-details');
});

Route::get('/contact-us', function () {
    return view('/contact-us');
});
