$('.homepage-banner').owlCarousel({
    loop: true,
    margin: 0,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
            margin: 0,
            dots: true
        },
        600: {
            items: 1,
            nav: false,
            margin: 0,
            dots: true
        },
        1000: {
            items: 1,
            nav: false,
            dots: true,
            margin: 0,
            loop: true
        }
    }
});


$('.product-full-image').owlCarousel({
    loop: true,
    margin: 0,
    autoplay: true,
    autoplayTimeout: 5000,
    slideTransition: 'linear',
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
            dots: true
        },
        600: {
            items: 1,
            nav: false,
            dots: true
        },
        1000: {
            items: 1,
            nav: false,
            dots: true,
            loop: true
        }
    }
});


$('.event-section-listing').owlCarousel({
    loop: true,
    margin: 25,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
            dots: true
        },
        600: {
            items: 1,
            nav: false,
            dots: false
        },
        1000: {
            items: 3,
            margin: 45,
            nav: false,
            dots: true,
            loop: true
        }
    }
});

$('.product-box-home').owlCarousel({
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
            dots: true
        },
        600: {
            items: 1,
            nav: false,
            dots: true
        },
        1000: {
            items: 1,
            nav: false,
            dots: true,
            loop: true
        }
    }
});

$('.other-products-owl').owlCarousel({
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
            dots: true
        },
        600: {
            items: 1,
            nav: false,
            dots: true
        },
        1000: {
            items: 4,
            nav: true,
            dots: false,
            loop: true
        }
    }
});

$('.product-portfolio').owlCarousel({
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 2,
            nav: false,
            dots: true
        },
        600: {
            items: 4,
            nav: false,
            dots: true
        },
        1000: {
            items: 7,
            nav: false,
            dots: false,
            loop: true
        }
    }
});

$('.happenings-owl').owlCarousel({
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
            dots: true
        },
        600: {
            items: 2,
            nav: false,
            dots: true
        },
        1000: {
            items: 3,
            nav: false,
            dots: true,
            loop: false
        }
    }
});

$('.ach-section-owl').owlCarousel({
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: true,
            dots: false
        },
        600: {
            items: 2,
            nav: true,
            dots: false
        },
        1000: {
            items: 2,
            nav: true,
            dots: false,
            margin:40,
            loop: true
        }
    }
});


$('.happening-page-owl').owlCarousel({
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
            dots: true
        },
        600: {
            items: 2,
            nav: false,
            dots: true
        },
        1000: {
            items: 3,
            nav: true,
            dots: false,
            loop: false
        }
    }
});

$(document).ready(function() {
    $(".footer-dropdown").click(function() {
        $(".footer-dropdown-menu").toggle();
    });

});



// dropdown menu show hide code //
    $(".menu-show-plus").click(function() {
        $(".dropdown-menu").each(function() {
            $(this).toggle();

        });
    });
